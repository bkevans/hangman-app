const gulp = require('gulp');

const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const babel = require('gulp-babel');
const util = require('gulp-util');
const minify = require('gulp-babel-minify');
const cleanCSS = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');

const PATHS = {
  javascript: [
		'assets/scripts/mobile-detect.js',
		'assets/scripts/requests.js',
    'assets/scripts/hangman.js',
    'assets/scripts/app.js'
  ]
};

// Static Server + watching scss files
gulp.task('serve', ['sass'], function () {

  browserSync.init({
		proxy: {
			target: 'http://localhost/~ben.evans/_APPS/hangman_app/dist',
			ws: true
		}
	});

  gulp.watch('assets/styles/styles.scss', ['sass']);
});

gulp.task('sass', function () {
  return gulp
		.src('assets/styles/styles.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(sourcemaps.init())
		.pipe(process.env.NODE_ENV === 'prod' ? cleanCSS({
						compatibility: 'ie8'
				  }) : util.noop())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./dist/css'))
		.pipe(browserSync.stream());
});

// Concatenate & Minify JS
gulp.task('scripts', function () {
  return gulp
		.src(PATHS.javascript)
		.pipe(concat('app.js'))
		.pipe(babel({ presets: ['es2015'] }))
    .pipe(process.env.NODE_ENV === 'prod' ? minify({ mangle: { keepClassName: true } }) : util.noop())
		.on('error', function(err) {
			util.log(util.colors.red('[Error]'), err.toString());
		})
		.pipe(gulp.dest('dist/js'))
		.pipe(browserSync.stream());
});

gulp.task('sass:watch', function () {
  gulp.watch('./assets/styles/styles.scss', ['sass']);
});

// Watch Files For Changes
gulp.task('watch', function () {
  gulp.watch('assets/scripts/*.js', ['scripts']);
  gulp.watch('assets/styles/**/*.scss', ['sass']);
  gulp.watch("**/*.html").on('change', browserSync.reload);
});

// Default Task
gulp.task('default', ['serve', 'sass', 'scripts', 'watch']);






