
![Hangman App Logo](dist/images/hangman_logo_wide.svg)

#### [Live Demo](http://hangman.bkevans.com)

An application based on the Hangman Guessing game. Users can guess random letters and try to figure out the word or phrase. 5 missed attempts result in losing the game.   

## Getting Started

### Prerequisites

Make sure that you have [Node.js](https://nodejs.org/en/) and [Yarn](https://yarnpkg.com/en/) installed.

To check the versions you have installed, run:  

```
node -v && yarn -v
```

### Installing

First you'll need to clone the source files onto your local machine. 

To clone you can run:

```
git clone https://bkevans@bitbucket.org/bkevans/hangman-app.git
```

Once cloned, move into the app directory:

```
cd hangman-app/
```

Now you'll need to install of the necessary dependencies to run the app locally using Yarn: 

```
yarn install
```

With all of the dependencies installed, you'll need to enter the path to your localhost for browser sync. This is assuming that you have a localhost environment set up on your machine. 

Add your localhost URL path for this project by editing *gulpfile.js*:

		browserSync.init({
			proxy: {
				target: 'YOUR_LOCALHOST_PATH_TO_PROJECT',
				ws: true
			}
		});

Now you're ready to run a development build and spin up a development server for the app by running: 

```
yarn dev
```

This will build the app, and automatically open the app in your default browser. The app is using browser sync for hot reloading on save.

Now that the app is up and running locally, you can crack open the /src directory and play around with the code or styling. Changes will be rendered in the browser immediately as long as the development server is running. 

To shut down the dev server, simply press [cntrl + c] 

## Deployment

This project uses a Bitbucket deployment pipeline to deploy to an S3 bucket. 

In order to use this method, you'll need to set up an S3 bucket, generate a role with keys and write privelages for your S3 bucket, and set up the keys in your Bitbucket repo. A good guide for setting this up can be found [here](https://medium.com/@mabdullahabid/how-to-use-bitbucket-pipelines-for-continuous-deployment-to-amazon-s3-aeb4b3e1f282)

Once you have configured your S3 bucket and added your access keys to your bitbucket repo, simply edit the *bitbucket-pipelines.yml* file in this project with the region and endpoint for your S3 bucket.

The file is currently set up to deploy the /dist directory whenever a git commit is made to the master branch and pushed to the repository.

To create a production build of this app stop your dev server (if it is still running [cntrl + c], and run:  

```
 yarn build
```

A production build will clean and minify JavaScript and CSS in the dist directory of the app. 

Simply commit your changes and push to your repo's master branch to initiate a deployment to your S3 bucket. Once complete you will have an updated app version that you can share with the world.  

## Built With

* [ES6 JavaScript](http://es6-features.org/) - Good Ol' Vanilla JavaScript
* [Gulp](https://gulpjs.com/) - Used for task automation
* [Babel](https://babeljs.io/) - As a JavaScript compiler
* [Sass](https://sass-lang.com/) - To make things look interesting

## License

This project is licensed under the MIT License

## Acknowledgments

This project was initially built while participating in an excellent [Udemy course](https://www.udemy.com/modern-javascript/) provided by Andrew Mead. A huge thank you to Andrew for sharing his knowledge and helping others grow their developement toolkit!