const guessesEl = document.querySelector('#guesses');
const puzzleEl = document.querySelector('#puzzle');
const messageEl = document.querySelector('#message');
const promptEl = document.querySelector('#prompt');

let game;

window.addEventListener('keypress', e => {
	if (game.status !== 'playing') {
		e.preventDefault();
	} else {
		const guess = String.fromCharCode(e.charCode);
		game.makeGuess(guess);
		render();
	}
});

const render = () => {
	puzzleEl.innerHTML = '';
	guessesEl.innerHTML = '';
	messageEl.innerHTML = game.statusMessage;

	game.puzzle.split('').forEach(letter => {
		const letterEl = document.createElement('span');
		letterEl.textContent = letter;
		puzzleEl.appendChild(letterEl);
	});

	game.missedLetters.forEach(letter => {
		const letterEl = document.createElement('span');
		letterEl.textContent = letter;
		guessesEl.appendChild(letterEl);
	});

	promptEl.textContent =
		game.status !== 'playing' ? '' : 'Press a letter key to make a guess.';
};

const startGame = async () => {
	const puzzle = await getPuzzle('2');
	game = new Hangman(puzzle, 5);
	render();
};

document.querySelector('#reset').addEventListener('click', startGame);

let deviceIs = new MobileDetect(window.navigator.userAgent);
if (deviceIs.mobile()) {
	messageEl.textContent = `Mobile devices are not supported.`;
	promptEl.textContent = `Please visit on a laptop or desktop to play this game.`;
	document.querySelector('#reset').remove();
} else {
	startGame();
}
